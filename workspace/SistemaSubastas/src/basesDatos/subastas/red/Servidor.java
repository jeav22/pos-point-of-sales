/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesDatos.subastas.red;

import bases.Datos.subastas.BaseEstructura.Link;
import bases.Datos.subastas.BaseEstructura.LinkListPuja;
import bases.Datos.subastas.BaseEstructura.LinkListSubasta;
import bases.Datos.subastas.BaseEstructura.LinkListUsuario;
import bases.Datos.subastas.controlador.Controlador;
import basesDatos.subastas.modelo.Subasta;
import basesDatos.subastas.modelo.Usuario;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.InputMismatchException;
import javax.swing.JTextArea;
import gui.ServidorInterfaz;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
* Clase Servidor: Es la clase que tiene acceso a lo que hace el usuario
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class Servidor extends Thread{
    /**
     * atributos de la clase servidor:
 * socket: oobjeto de la clase Socket
 * servidor : objeto de ServerSocket
 * escritor: objeto de PrintWriter para escribir
 * lector: objeto de BufferedReader para leer datos
 * controlador : objeto de la clase controlador
 * comandos : comandos que entraran al usuario
 * respuesta: respuesta del servidor
 * mensaje: mensaje del servidor
 * area:espacio de texto de interfaz
 * incremento : valor a incrementar credito
 */  
     public static Socket socket=null;
     private ServerSocket servidor=null;
     private BufferedReader lector=null;
     private PrintWriter escritor=null;
     private static Controlador controlador;
     private static String [] comandos;
     private static String aux,mensaje=null,respuesta;
     private JTextArea area;
     public ServidorInterfaz sI;
     
/**
*Metodo: Obtiene el controlador que es el encargado de conectar la vista con la ejecución 
* @return controlador
*/
     public Controlador getControlador() {
        return controlador;
    }
     /**
 * Metodo serControlador: Permite enviar lo referente a la vista al controlador. 
 * @param controlador recibe el controlador de la aplicación como parametro 
 */
    public void setControlador(Controlador controlador) {
        this.controlador = controlador;
    }
    /**
 * Metodo crearUsuario: Este método le permite crear un usuario para el Sistema de Subastas.
     * @return Retorna un valor booleano el cual indica si el proceso tuvo o no exito
 * @throws InputMismatchException  Excepcion para indicar que la entrada recibida por el usuario no coincide con el patron de tipo esperado.
     * @throws java.io.IOException
 */
    public boolean crearUsuario() throws InputMismatchException, IOException {
        String nombre= comandos[1];   
        if (controlador.buscarUsuario(nombre) == null) {
            double credito = Double.parseDouble(comandos[2]);
            Usuario usuario = new Usuario(nombre, credito,true);
            controlador.guardarUsuario(usuario);
            controlador.listarUsuarios();
            controlador.guardarSistema();
            return true;
        }else {
            return false;
        }
    } 
/**
 * Método listarUsuario: Permite mostrar al Usuario las personas que estan dentro del sistema de subastas.
 * @param usuarios Indica en el metodo los usuarios a listar.
 * @return lista de usuarios
 */
    public String listarUsuario(LinkListUsuario usuarios) {        
        String us="";
        Link current = usuarios.first;
            if (usuarios.isEmpty() != true) {
                while(current != null){
                    us=us+current.usuario.getNombre()+",";
                    current = current.next;
                }
                System.out.println(us);
                sI.dlm=new DefaultListModel();
                String[] m=us.split(",");
                for (String i : m) {
                    sI.dlm.addElement(i);
                }
                sI.listaClientes.setModel(sI.dlm);
                return us;
            } else {
                us= "No hay usuarios registrados";
                return us;
            }
    }
    /**
 * Metodo actualizarCredito: Le permite al usuario actualizar su credito, ya que puede que se le haya agotado el dinero y el quiera seguir participando en las subastas este metodo le permitira ingresar mas dinero a su credito.
     * @return Retorna un valor booleano el cual indica si el proceso tuvo o no exito
     * @throws java.io.IOException
 */
    public boolean actualizarCredito() throws IOException {
        String nombre= comandos[1];   
        Usuario usuario = controlador.buscarUsuario(nombre);  
        double incremento = Double.parseDouble(comandos[2]);
        controlador.guardarSistema();
        return usuario.incrementarCredito(incremento);
    }
 /**
 * Metodo crearSubasta: Permite al usuario crear una Subasta a través de los pasos correspondientes pedidos por consola.  
     * @throws java.io.IOException
 */
    public void crearSubasta() throws IOException {
        String nombre= comandos[1]; 
        Usuario usuario = controlador.buscarUsuario(nombre);  
        String producto = comandos[2];
        Subasta subasta = new Subasta(producto, usuario);
        controlador.guardarSubasta(subasta);
        controlador.guardarSistema();
    }   
    /**
 * Método ListarSubastas: Permite observar las Subastas creadas por el usuario con el objetivo de brindarle información acerca de las Subastas en las que quiera participar.
 * @param subastas Recibe como parametro una coleccion de tipo ArrayList de objetos tipo subasta para mostrar las subastas almacenadas.
     * @return Retorna un string con la lista de los nombres de las subastas separados por comas ","
 */
    public String listarSubastas(LinkListSubasta subastas) {
        String listaSubastas="";
        Link current = subastas.first;
            if (subastas.isEmpty() != true) {
                while(current != null){
                    if(listaSubastas.equals("")){
                        listaSubastas=current.subasta.getProducto();
                    }else{
                        listaSubastas = listaSubastas+","+current.subasta.getProducto();
                    }
                    current = current.next;
                }
            } else {
                listaSubastas="No hay subastas disponibles ";
            }
            return listaSubastas;
    }
    
    /**
 * Método pujarSubastas: Mediante este metodo se le permitira al usuario pujar por una subasta en especifico, es decir, entrar a participar en una subasta.
     * @return si logra pujar retorna true sino false
     * @throws java.io.IOException
 */
    public boolean pujarSubastas() throws IOException {
        String nombre= comandos[1];   
        Usuario usuario = controlador.buscarUsuario(nombre);  
        String producto = comandos[2];
        Subasta subasta = controlador.buscarSubasta(producto);
        double valor = Double.parseDouble(comandos[3]);
        controlador.guardarSistema();        
        return controlador.pujar(usuario, subasta, valor);
    }
    /**
 * Método CerrarSubastas: Permite al propietario de la Subasta cerrarla de manera que no se realicen mas pujas por ella. 
     * @return true si logra cerrar la subasta
     * @throws java.io.IOException
 */
    public boolean cerrarSubastas() throws IOException {        
        String nombre= comandos[1];   
        Usuario usuario = controlador.buscarUsuario(nombre);  
        String producto = comandos[2];
            System.out.println(producto);
        Subasta subasta = controlador.buscarSubasta(producto);
        if(subasta!=null && subasta.getPropietario().equals(usuario)){
            controlador.guardarSistema();
            return controlador.cerrarSubasta(subasta);
        }else{
            return false;
        }
    }    
    /**
 * Método listarSubastasCreadas: Permite observar cada una de las subastas en las que participo el usuario, arrojando informacion de las subastas creadas y las ubastas ganadas.
     * @return la lista de subastas
 */
    public String listarSubastasCreadas() {
        String nombre= comandos[1];   
        Usuario usuario = controlador.buscarUsuario(nombre);  
        String subastasCreadas="";
        LinkListSubasta subastas = this.controlador.listarPujas();
        Link current= subastas.first;
        while(current!= null){
            if (current.subasta.getPropietario().equals(usuario)){
                if(subastasCreadas==""){
                    subastasCreadas = current.subasta.getProducto();
                }else{
                    subastasCreadas = subastasCreadas+","+current.subasta.getProducto();
                }
            }
            current=current.next;
        }
        if(subastasCreadas.equals("")){
            subastasCreadas= " ,0, ,No hay subastas creadas, /-/";
            return subastasCreadas;
        }else{
            return subastasCreadas;
        }
    }
    /**
 * Método listarSubastasGanadas: Permite observar cada una de las subastas en las que participo el usuario, arrojando informacion de las subastas creadas y las ubastas ganadas.
     * @return subastas ganadas
 */
    public String listarSubastasGanadas() {
        String nombre= comandos[1];   
        Usuario usuario = controlador.buscarUsuario(nombre);
        String subastasGanadas="";
        LinkListSubasta subastas = this.controlador.listarPujas();
        Link current= subastas.first;  
        while(current!=null){
            if (current.subasta.getGanador().getUsuario().equals(usuario)) {
                if(subastasGanadas.equals("")){	
                    subastasGanadas= current.subasta.getProducto();
                }else{
                    subastasGanadas= subastasGanadas+","+current.subasta.getProducto();
                }
            }
            current=current.next;
        }
        if(subastasGanadas.equals("")){
            subastasGanadas= "No hay subastas ganadas/-/No hay subastas ganadas";
            return subastasGanadas;
        }else{
            return subastasGanadas;
        }
    }
    /**
 * Método listarPujasUsuario: permite mostrar las pujas que ha hecho el usuario
 */
    public void listarPujasUsuario() {
        
    }
    /**
 * Método cerrarSesion: permite cerrar la sesion del servidor.
     * @return si se puede cerrar la sesion del usuario
     * @throws java.io.IOException
 */
    public boolean cerrarSesion() throws IOException {
        String nombre= comandos[1];
        Usuario usuario= controlador.buscarUsuario(nombre);
        controlador.guardarSistema();
        return controlador.cerrarUsuario(usuario);
    }
     /**
 * Método leer: permite leer el mensaje del usuario.
 */
    public void leer(){
        Thread leer = new Thread(new Runnable() {
            public void run() {
                try{                    
                    //Get the return message from the server
                    lector= new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while(true){  
                        controlador.guardarSistema();
                        aux=lector.readLine();
                        mensaje= aux;
                        procesar();
                        if(mensaje!=null){
                            area.append("Recibido: "+aux+"\n");
                        }
                    }                    
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        leer.start();
    }
    /**
 * Método escribir: permite escribir el mensaje del usuario.
 */
    public void escribir(){
        Thread escribir = new Thread(new Runnable() {
            public void run() {
                try{  
                    //Send the message to the server
                    escritor= new PrintWriter(socket.getOutputStream(),true);
                    if(!"null".equals(respuesta)){
                        escritor.println(respuesta);
                        area.append("Respuesta: "+respuesta+"\n");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        escribir.start();
    }
   /**
 * Método procesar: permite procesar la solicitud al servidor.
     * @throws java.io.IOException
 */ 
    public void procesar() throws IOException{
        //aqui debe hacerse el proceso de la solicitud
        //cada solicitud de informacion del usuario debe pedirse al servidor
        if(mensaje!= null){
            comandos = mensaje.split(",");

            if (comandos[0].equals("0")) {
               Usuario usuario= controlador.buscarUsuario(comandos[1]);
               if(usuario!=null)
                   respuesta = Boolean.toString(controlador.iniciarSesion(usuario));
               else
                   respuesta="false";
           } else if (comandos[0].equals("1")) {
               respuesta = Boolean.toString(controlador.crearUsuario());
           } else if (comandos[0].equals("2")) {
               respuesta= controlador.listarUsuarios();
           } else if (comandos[0].equals("3")) {
               respuesta = Boolean.toString(controlador.actualizarCredito());
           } else if (comandos[0].equals("4")) {
               controlador.crearSubasta();
                   respuesta=controlador.listarSubastas();
           } else if (comandos[0].equals("5")) {
               respuesta = controlador.listarSubastas();
           } else if (comandos[0].equals("6")) {
               respuesta = Boolean.toString(controlador.pujarSubastas());
           } else if (comandos[0].equals("7")) {
                respuesta = Boolean.toString(controlador.cerrarSubastas());
           } else if (comandos[0].equals("8")) {
               respuesta = controlador.mostrarSubastasCreadas();
           } else if (comandos[0].equals("9")) {
               respuesta = controlador.mostrarSubastasGanadas();
           } else if (comandos[0].equals("10")) {
               controlador.mostrarPujasUsuario();
                   respuesta="true";
           } else if (comandos[0].equals("11")) {
              respuesta =Boolean.toString(controlador.cerrarUsuario(controlador.buscarUsuario(comandos[1])));
           }else if(comandos[0].equals("12")) {
               Subasta subasta = controlador.buscarSubasta(comandos[1]);
               if(subasta!=null){
                respuesta=subasta.getProducto()+","+subasta.getGanador().getValor()+","+subasta.isEstado()+","+subasta.getGanador().getUsuario().getNombre()+","+subasta.getPropietario().getNombre()+"/-/"+controlador.mostrarPujasSubasta();
               }else{
                   respuesta=" ,0, ,No hay ganador, /-/ ";
               }
           }else{
                   respuesta="false";
           }
        }else{
                respuesta="null";
        }
        actualizarInterfaz();
        escribir();
    }
    
    public void actualizarInterfaz(){
        int aux=sI.listaClientes.getSelectedIndex();
        if(aux<=0){
            sI.listaClientes.setSelectedIndex(aux+1);
        }else{
            sI.listaClientes.setSelectedIndex(aux-1);
        }
        sI.listaClientes.setSelectedIndex(aux);
    }
    /**
 * Método servidor: permite leer las solicitudes y mantener activo el servidor
 */
    public Servidor() {        
        sI = new ServidorInterfaz();
        area = sI.area;
                
        sI.cerrarSesion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controlador.cerrarUsuario(controlador.buscarUsuario(sI.listaClientes.getSelectedValue().toString()));
                actualizarInterfaz();
            }
        });
        
        sI.listaClientes.addListSelectionListener( new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Usuario usuario= controlador.buscarUsuario(sI.listaClientes.getSelectedValue().toString());
                sI.nombre.setText(usuario.getNombre());
                sI.credito.setText(""+usuario.getCredito());
                sI.estado.setText(""+usuario.istEstado());
                if(!usuario.istEstado()){
                    sI.cerrarSesion.setEnabled(false);
                }else{
                    sI.cerrarSesion.setEnabled(true);
                }
                
                mensaje=" ,"+sI.listaClientes.getSelectedValue().toString();
                comandos= mensaje.split(",");
                String m=listarSubastasCreadas();
                DefaultListModel dlm = new DefaultListModel();
                String[] r= m.split(",");
                for (String i : r) {
                    dlm.addElement(i);
                }
                sI.subastasCreadas.setModel(dlm);
                
                m =listarSubastasGanadas();
                dlm = new DefaultListModel();
                r= m.split(",");
                for (String i : r) {
                    dlm.addElement(i);
                }
                sI.pujasGanadas.setModel(dlm);
            }
        });
                
       Thread principal = new Thread(new Runnable() {
           public void run() {
               try {       
                    servidor = new ServerSocket(2500);
                    InetAddress localHost = InetAddress.getLocalHost();
                    area.append("Servidor iniciado escuchando en el puerto 2500 con ip: " + localHost.getHostAddress()+"\n");
                    //Servidor siempre se encunetra activo. Hecho mediante el uso del ciclo infinito while(true)
                    while (true) {
                        //Leyendo la solicitud del cliente
                        socket = servidor.accept();
                        area.append("Nueva Conexion: "+socket.getRemoteSocketAddress()+"\n");
                        leer();
                    }
               }catch(Exception e){
                   e.printStackTrace();
               }
           }
       });
       principal.start();
    }

    public String listarPujasSubasta() {
        Subasta subasta = controlador.buscarSubasta(comandos[1]);
        LinkListPuja pujas = subasta.getPujas();
        Link current = pujas.first;
        String m="";
        if(current!=null){
            while (current!=null) {                
                if(m==""){
                    m="                 "+current.puja.getUsuario().getNombre()+"                                                   "+current.puja.getValor();
                }else{
                    m=m+",                 "+current.puja.getUsuario().getNombre()+"                                                   "+current.puja.getValor();
                }
                current=current.next;
            }
        }else{
            m="No \t Hay \t Pujas";
        }
        return m;
    }
}
