/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesDatos.subastas.red;

import gui.ClienteInterfaz;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
/**
* Clase Cliente: Es la clase que contiene el contenido que digita el usuarioo en la plataforma
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class Cliente extends Thread{
    /**
 * Atributos de la clase Cliente: 
 * socket: objeto de la calse socket
 * escritor: objeto de la clase PrintWriter para escribir
 * lector : objeto de BufferedReader para leer datos
 * cl : objeto de la clase ClienteInterfaz
 */  
     private static Socket socket;
     private PrintWriter escritor;
     private BufferedReader lector = null;
     
     private ClienteInterfaz cI;
     /**
 * metodo leer:  permite leer lo que el usuario digita 
 */
    public void leer() {
    	Thread leer = new Thread(new Runnable() {
            public void run() {
                try {
                    //Get the return message from the server
                    lector = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while (true) {
                    String m= lector.readLine();
                    cI.respuesta =m;
                    cI.procesar(cI.seteado,cI.respuesta);
                    cI.area.append("servidor: " + cI.respuesta + "\n");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    	});
    	leer.start();
	}
/**
 * metodo escribir:  permite escribir al usuario diferentes opciones 
 */
	public void escribir() {
    	Thread escribir = new Thread(new Runnable() {
            public void run() {
                try {
                    //Send the message to the server
                    escritor = new PrintWriter(socket.getOutputStream(), true);
                    escritor.println(cI.seteado);                    
                    cI.area.append("Cliente: " + cI.seteado+ "\n");
                                                            
                    cI.iniciar.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.usuario=JOptionPane.showInputDialog(null, "Digite su usuario", "Iniciar sesion",1);
                            cI.seteado="0,"+cI.usuario;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+"\n");
                        }
                    }); 
                    cI.registrarse.addActionListener(new ActionListener() {
                        @Override
                        /**
 * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        public void actionPerformed(ActionEvent e) {                
                            cI.usuario=JOptionPane.showInputDialog(null, "Digite su usuario", "Registro 1",1);
                            cI.credito=Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese su credito inicial", "Registro 2", 1));
                            if(cI.credito<0){
                                cI.credito=Double.parseDouble(JOptionPane.showInputDialog(null, "Su credito inicial no puede ser negativo","registro 2",2)); 
                            }
                            cI.seteado="1,"+cI.usuario+","+cI.credito;
                            cI.aumentarCredito.setEnabled(true);
                            cI.cerrar.setEnabled(true);
                            cI.iniciar.setEnabled(false);
                            cI.registrarse.setEnabled(false);
                            cI.crearSubasta.setEnabled(true);
                            cI.subastasGanadas.setEnabled(true);
                            cI.subastasCreadas.setEnabled(true);
                            cI.setTitle("::: Sistema Subasta ::: - "+cI.usuario);
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                        }
                    });
                    cI.listaSubastas.addListSelectionListener(new ListSelectionListener() {
                        @Override
                        public void valueChanged(ListSelectionEvent e) {   
                            if(cI.listaSubastas.getSelectedIndex()>-1){
                                String m= cI.listaSubastas.getSelectedValue().toString();
                                cI.seteado="12,"+m;
                                escritor.println(cI.seteado);
                                cI.area.append("Cliente: " + cI.seteado+ "\n");                       
                            }
                        }
                    });
                    cI.aumentarCredito.addActionListener(new ActionListener() {
                        @Override
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
                        * @param e la accion del usuario
                        */
                        public void actionPerformed(ActionEvent e) {
                            cI.credito=Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese su nuevo credito", "Aumentar credito", 1));
                            if(cI.credito<0){
                                cI.credito=Double.parseDouble(JOptionPane.showInputDialog(null, "Su credito no puede ser negativo","Aumentar credito",2));
                            }
                            cI.seteado="3,"+cI.usuario+","+cI.credito;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+"\n");
                        }
                    });
                    cI.cerrar.addActionListener(new ActionListener() {
                        @Override
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        public void actionPerformed(ActionEvent e) {
                            cI.cerrar.setEnabled(false);
                            cI.aumentarCredito.setEnabled(false);
                            cI.subastasGanadas.setEnabled(false);
                            cI.subastasCreadas.setEnabled(false);
                            cI.crearSubasta.setEnabled(false);
                            cI.iniciar.setEnabled(true);
                            cI.registrarse.setEnabled(true);
                            cI.setTitle("::: Sistema Subasta :::");
                            cI.seteado="11,"+cI.usuario;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+"\n");
                        }
                    });
                    cI.crearSubasta.addActionListener(new ActionListener() {
                        @Override
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        public void actionPerformed(ActionEvent e) {
                            cI.subasta=JOptionPane.showInputDialog(null, "Ingrese el nombre del producto a subastar", "Crear subasta", 1);
                            if(cI.subasta.isEmpty()){
                                cI.subasta=JOptionPane.showInputDialog(null, "No puede subastar un espacio vacio\nDigite el nombre del producto","Crear subasta", 2);
                            }
                            cI.seteado="4,"+cI.usuario+","+cI.subasta;
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                            cI.todas.getAction();
                        }
                    });
                    cI.pujar.addActionListener(new ActionListener() {
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.subasta=cI.listaSubastas.getSelectedValue().toString();
                            cI.credito=((SpinnerNumberModel)cI.valorPuja.getModel()).getNumber().doubleValue();
                            cI.seteado="6,"+cI.usuario+","+cI.subasta+","+cI.credito;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                        }
                    });
                    cI.cerrarSubasta.addActionListener(new ActionListener() {
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.seteado="7,"+cI.usuario+","+cI.listaSubastas.getSelectedValue().toString();
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                        }
                    });      
                    cI.subastasGanadas.addActionListener(new ActionListener() {
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.seteado="9,"+cI.usuario;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                        }
                    });
                    cI.subastasCreadas.addActionListener(new ActionListener() {
                        /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.seteado="8,"+cI.usuario;
                            escritor.println(cI.seteado);
                            cI.area.append("Cliente: " + cI.seteado+ "\n");
                        }
                    });
                    cI.todas.addActionListener(new ActionListener() {
                       /**
                        * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            cI.seteado="5";
                            escritor.println(cI.seteado);
                            String[] r= cI.respuesta.split(",");
                            cI.area.append("Cliente: " + cI.seteado+"\n");
                        }
                    });
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    	escribir.start();
	}
        /**
   * metodo cliente : permite activar la interfaz del cliente para leer y escribir datos
 */
    public Cliente() {
        cI = new ClienteInterfaz();
        
        Thread principal = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket("localhost", 2500);
                    leer();
                    escribir();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        principal.start();
    }       
}
