package basesDatos.subastas.ejecucion;

import bases.Datos.subastas.controlador.Controlador;
import basesDatos.subastas.modelo.SistemaSubasta;
import basesDatos.subastas.red.Cliente;
import basesDatos.subastas.red.Servidor;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
* Clase Main: Es la encargada de ejecutar la aplicación. Para correr la aplicación se debe ejecutar desde esta clase. 
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class Main {
    /**
     * atributos de la clase Main
     * cliente: objeto de la clase Cliente
     */
    private static Cliente cliente;
/**
     * metodo main : corre el programa
     * @param args argumentos predeterminados
     */
    public static void main(String[] args) {

        SistemaSubasta modelo= new SistemaSubasta();
        Servidor servidor= new Servidor();
        Controlador controlador = new Controlador(servidor, modelo);
        System.out.println("Hola mundo");
        
        try {         
            controlador.cargarSistema();
            controlador.listarUsuarios();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        cliente = new Cliente();            
    }
}