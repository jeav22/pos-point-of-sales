package basesDatos.subastas.persistencia;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import basesDatos.subastas.modelo.SistemaSubasta;
/**
* Clase Read: Clase que permite a través de la interfaz File  leer nuestro archivo donde se almacena la informacion de las subastas.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/
public class Read {
/**
 * Atributos de la clase Read: 
 * File: Objeto de la interfaz File para abrir un flujo de datos hacia un archivo. 
 * Input: Objeto de la interfaz File es la entrada para el flujo de datos abierto hacia el archivo. 
 */    
	private FileInputStream file;
	private ObjectInputStream input;
/**
 * Metodo abrir: Permite abrir el flujo de datos para permitir la comunicacion con el archivo.
 * @throws IOException Excepcion para indicar un error a la hora de abrir el archivo.
 */	
	public void abrir() throws IOException{
		this.file=new FileInputStream("database.bin");
		this.input=new ObjectInputStream(this.file);
    }
/**
 * Metodo cerrar: Permite cerrar el flujo de datos para termianr la conexión con el archivo.
 * @throws IOException Excepcion para indicar un error a la hora de cerrar el archivo.
 */
        
	
	public void cerrar() throws IOException{
		if(this.input != null){
			this.input.close();
		}
	}
/**
 * Método leer: Nos permite leer los objetos de tipo SistemaSubasta almacenenados en el archivo.
 * @return baseDatos. Nos retorna los objetos leidos dentro del archivo.  
 * @throws ClassNotFoundException excepcion de tipo Clase no encontrada
 * @throws IOException Excepcion que indica un error a la hora de leer el archivo que almacena las subastas
 */        
	
	public SistemaSubasta leer() throws ClassNotFoundException, IOException{
		SistemaSubasta baseDatos=null;
		if(this.input != null){
			baseDatos= (SistemaSubasta) this.input.readObject();
		}
		return baseDatos;		
	}
	
}
