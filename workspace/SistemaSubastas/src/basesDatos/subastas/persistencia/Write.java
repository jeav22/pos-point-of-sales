package basesDatos.subastas.persistencia;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import basesDatos.subastas.modelo.SistemaSubasta;

/**
* Clase Write: Clase que permite a través de la interfaz File  escribir nuestro archivo donde se almacena la informacion de las subastas y los usuarios.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/
public class Write {
  
/**
 * Atributos de la clase Read: 
 * File: Objeto de la interfaz File para abrir un flujo de datos hacia un archivo. 
 * Input: Objeto de la interfaz File es la entrada para el flujo de datos abierto hacia el archivo. 
 */    
	private FileOutputStream file;
	private ObjectOutputStream output;
/**
 * Metodo abrir: Permite abrir el flujo de datos para permitir la comunicacion con el archivo.
 * @throws IOException Excepcion que indica un error a la hora de abrir el flujo de datos que comunica con el archivo.
 */
	public void abrir() throws IOException{
		this.file=new FileOutputStream("database.bin");
		this.output=new ObjectOutputStream(this.file);
	}
/**
 * Metodo cerrar: Permite cerrar el flujo de datos para termianr la conexión con el archivo.
 * @throws IOException Excepcion que indica un error a la hora de cerrar la conexion con el archivo.
 */
	public void cerrar() throws IOException{
		if(this.output != null){
			this.output.close();
		}
	}
/**
 * Método escribir: Nos permite escribir dentro del archivo.
 * @param baseDatos este parametro es utilizado debido a que en a BaseDatos es donde se realizara la escritura 
 * @throws IOException  Excepcion que indica un error a la hora de escribir en el archivo.
 */	
	public void escribir(SistemaSubasta baseDatos) throws IOException{
		if(this.output != null){
			this.output.writeObject(baseDatos);
		}
	}
	
}
