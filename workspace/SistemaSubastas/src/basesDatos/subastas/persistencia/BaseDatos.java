package basesDatos.subastas.persistencia;

import java.io.File;
import java.io.IOException;
import basesDatos.subastas.modelo.SistemaSubasta;

/**
* Clase BaseDatos: Clase que permite gestionar la información a través de lectura y escritura de archivos almacenando lo relacionado al sistema de subastas. 
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/

public class BaseDatos {
    /**
     * Atributos de la clase BaseDatos: 
     * Leer y escribir: Son objetos de la clase Read y Write. Se utilizaran para manipular el archivo donde gestionaremos nuestra informacion de Subastas.
     */
	private Read leer;
	private Write escribir;
    /**
     * Contructor de la clase BaseDatos
     */    
	public BaseDatos(){
	this.leer= new Read();
	this.escribir= new Write();
	}
    /**
     * Metodo leerSubasta: Nos permite leer las subastas almacenadas dentro del archivo.
     * @param archivo Indica en el metodo el archivo donde se leeran las subastas
     * @return retorna el contenido del archivo.
     */    
	
	public SistemaSubasta leerSubastas(String archivo) {
		File baseDatos= new File(archivo);
		SistemaSubasta base= null;
		//Si existe un archivo creado en la Base Datos
		if(baseDatos.exists()){
			try{
				leer.abrir();
				base = leer.leer();
				leer.cerrar();	
			}catch(IOException e){
				System.out.println("Debe revisar que el archivo existe");
			}catch(ClassNotFoundException e){
				System.out.println("No existe un objeto del tipo solicitado");
			}
		}else{
			                System.out.println(" La base de Datos no existe ");
		}
		return base;
	}
    /**
     * Método escribirSubastas: Permite introducir informacion dentro del archivo referente a las Subastas
     * @param modelo nuestro parametro es el sistema de subastas el cual vamos a almacenar en el archivo
     * @throws IOException Excepcion para indicar un error al escribir la Subastas en el archivo.
     */    
	public void escribirSubastas(SistemaSubasta modelo) throws IOException{
			this.escribir.abrir();
			this.escribir.escribir(modelo);
			this.escribir.cerrar();
	}
}
