package basesDatos.subastas.modelo;

import bases.Datos.subastas.BaseEstructura.LinkListPuja;
import java.io.Serializable;

/**
* Clase Subasta: contiene toda la informacion de las subastas que el usuario hara dentro del programa
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/

public class Subasta implements Serializable{
/**
 * Atributos de la clase Subasta:
 * producto: De tipo String, su modificador de acceso es privado.
 * estado: De tipo booleano, su modificador de acceso es privado. 
 * propietario: Objeto de la clase Usuario, su modificador de acceso es privado.
 * pujas: objeto de la clase link el cual manejara y almacenara las pujas.
 * ganador: Objeto de la clase Puja, su modificador de acceso es privado.
 */
    private String producto;
    private boolean estado;
    private Usuario propietario;
    private LinkListPuja pujas;
    private Puja ganador;
/**
 * Construtor de la clase Subasta
 * @param producto Indica en el contructor el producto de la subasta
 * @param propietario Indica en el contructor propietario de la subasta.
 */
	
    public Subasta(String producto, Usuario propietario) {
        this.producto = producto;
        this.propietario = propietario;
        this.estado = true;
        this.pujas = new LinkListPuja();
        this.ganador = new Puja(0.0, new Usuario(" No hay ganador ", 0,false));
    }

    
/**
 * Método getProducto: Permite obtener el producto de la subasta.
 * @return retorna el producto obtenido. 
 */
	
    public String getProducto() {
        return producto;
    }
/**
 * Método setProducto: Permite enviar el producto de la subasta.
 * @param producto Producto a enviar.
 */            
    public void setProducto(String producto) {
        this.producto = producto;
    }
/**
 * Método isEstado: Permite conocer el estado de la subasta. 
 * @return retorna el estado de la subasta, true si esta disponible o false si no lo esta. 
 */
    public boolean isEstado() {
        return estado;
    }
/**
 * Método setEstado: Permite enviar el estado de la subasta.
 * @param estado En el metodo nos permite conocer si la subasta esta dispoible o no.
 */        
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
/**
 * Método getPropietario: Permite obtener el propietario de la Subasta.
 * @return Retorna el nombre del propietario de la subasta.
 */
    public Usuario getPropietario() {
        return propietario;
    }
/**
 * Método setPropietario: Permite enviar el propietario de la Subasta.
 * @param propietario En el metodo indica el nombre del propietario a enviar
 */
    public void setPropietario(Usuario propietario) {
        this.propietario = propietario;
    }
/**
 * Método getPujas: Perimite mediante el uso de las listas saber la puja almacenada.
 * @return Retorna las pujas obtenidas que se almacenaron dentro de la lista. 
 */
    public LinkListPuja getPujas() {
        return pujas;
    }
/**
 * Método addPujas: Permite añadir pujas a la lista. 
 * @param puja Indica la puja a añadir.
 */        
    public void addPujas(Puja puja) {
        this.pujas.insertFirst(puja);
    }
/**
 * Método getGanador: Permite obtener el ganador en dicha puja es decir quien mas dinero invirtio en la subasta.
 * @return ganador
 */        
    public Puja getGanador() {
        return ganador;
    }
/**
 * Método setGanador: Permite enviar el ganador de dicha puja. 
 * @param ganador indicar en el metodo el ganador al cual se debe enviar.
 */        
    public void setGanador(Puja ganador) {
        this.ganador = ganador;
    }
/**
 * Método registrarPuja: Nos permite registrar la Puja dentro de la subasta para asi validarla y poder obtener un ganador pero en este caso validando la información de si es propietario o no.
 * @param valor este parametro hace referencia al valor del credito que tiene el usuario
 * @param usuario este parametro hace referencia al usuario que realiza la puja
 * @return un valor de tipo booleano en caso de que se pueda registrar la puja en la subasta o no.
 */        
	
    public boolean registrarPuja(double valor, Usuario usuario){
        //Precondicion
        if(this.ganador != new Puja(0.0, new Usuario("No hay", 0,false)) && usuario!=null){
            //Condiciones de aceptacion
            if(this.estado == true && valor >=1 && usuario.getCredito() > valor && 
                usuario != this.propietario && ganador.getValor() < valor){
                Puja puja = new Puja(valor, usuario);
                if(this.pujas.insertFirst(puja)){
                    ganador = puja;
                    return true;
                }
                return false;
            }	//Lanzar Excepcion		
        }
        else{
            //Caso primera puja
            if( usuario!= null && this.estado == true && usuario.getCredito() > valor && 
                usuario != this.propietario ){
                Puja puja = new Puja(valor, usuario);
                if(this.pujas.insertFirst(puja))
                    return true;
            }
        }//Lanzar Excepcion
        return false;	
    }
/**
 * Método RegistarPuja: Nos permite registrar la Puja dentro de la subasta para asi validarla y poder obtener un ganador.
 * @param usuario permite en el metodo registrar el nombre del usuario que realizo la puja.
 * @return un valor de tipo booleano en caso de que se pueda registrar la puja en la subasta o no.
 */        
	
    //Sobrecarga de metodos (Polimorfismo)
    public boolean registrarPuja(Usuario usuario){
        //Precondicion
        if(ganador != new Puja(0.0, new Usuario("No hay", 0,false))){
            if(this.estado == true && usuario.getCredito() > (ganador.getValor() +1 ) && usuario != this.propietario){
                Puja puja = new Puja((this.ganador.getValor() +1 ), usuario);
                if(this.pujas.insertFirst(puja)){
                    ganador = puja;
                    return true;
                }
            }
        }else{//Caso primera puja
            if( this.estado == true && usuario.getCredito() > 1 && usuario != this.propietario){
                Puja puja = new Puja(1, usuario);
                if(this.pujas.insertFirst(puja)){
                    ganador = puja;
                    return true;
                }
            }
        }
        return false;
    }
/**
 * Método cerrarSubasta: Permite a través de un tipo de dato booleano cerrar la Subasta.
 * @return retonarna true al cerrar la subasta.
 */        
	
    public boolean cerrarSubasta(){
        this.estado=false;
        //Decrementar el valor al ganador
        this.ganador.getUsuario().decrementarCredito(this.ganador.getValor());

        //Incrementar al propietario
        this.propietario.incrementarCredito(this.ganador.getValor());
        return true;
    }	
    public int getTamaño() {
        return getProducto().length()*2 + 2 + 4 + 1;
    }
}