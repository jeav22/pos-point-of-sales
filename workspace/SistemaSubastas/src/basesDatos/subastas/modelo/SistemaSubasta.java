package basesDatos.subastas.modelo;

import bases.Datos.subastas.BaseEstructura.Link;
import bases.Datos.subastas.BaseEstructura.LinkListPuja;
import java.io.Serializable;
import bases.Datos.subastas.BaseEstructura.LinkListUsuario;
import bases.Datos.subastas.BaseEstructura.LinkListSubasta;
/**
* Clase Sistema Subasta: Contiene toda la información del Sistema de Subastas e implementa métodos de la interfase Serializable . 
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/

public class SistemaSubasta implements Serializable{ 
/**
 * Atributos de la clase SistemaSubasta:
 *usuarios: objeto de link que representa a los usuarios almacenados.
 * subastas: objeto de link que representa a las subastas almacenados.
 */    
	private LinkListUsuario usuarios;
	private LinkListSubasta subastas;
        
/**
 * Contructor de la clase SistemaSubastas
 * SistemaSubasta
 */
	public SistemaSubasta(){
	 this.usuarios = new LinkListUsuario();
	 this.subastas = new LinkListSubasta();
	}
/**
 * Método getUsuario: Permite obtener los objetos de tipo Usuario de la coleccion.
 * @return objeto de tipo usuario almacenado en la coleccion 
 */        

	public LinkListUsuario getUsuarios() {
		return usuarios;
	}
/**
 * Método addUsuario: Permite añadir objetos de tipo Usuario a la lista.
 * @param usuario Indica en el metodo el usuario a añadir
 */  
public void addUsuarios(Usuario usuario) {
    
		this.usuarios.insertFirst(usuario);
	}
/**
 * Método getSubasta: Permite obtener los objetos de tipo Subasta a la respectiva lista. 
 * @return objeto de tipo subasta almacenado en la lista. 
 */ 
	public LinkListSubasta getSubastas() {
		return subastas;
	}
/**
 * Método addUsuario: Permite añadir objetos de tipo Usuario a la lista correspondiente además permite agregar al propietario su correspondiente subasta.
 * @param subasta Nos indica en el caso del metodo la subasta a añadir
 */  
	public void addSubastas(Subasta subasta) {
		this.subastas.insertFirst(subasta);
		subasta.getPropietario().addPropietario(subasta);
	}
/**
 * Método mostrarSubastasParticipaUsuario: Permite mendiante la lista que almacena las subastas y con ayuda de las pujas realizadas por el usuario obtener el resutaldo de las subastas en las que participa.
 * @param usuario Indica en el metodo el usuario del cual se debe obtener la informacion de las subastas en las que participo. 
 * @return el resultado de las subastas en las que participo el usuario
 */	
    public LinkListSubasta mostrarSubastasParticipaUsuario(Usuario usuario){
	LinkListSubasta resultado = new LinkListSubasta();
        LinkListPuja puja = new LinkListPuja();
        //Falta cambiar el metodo
        Link current = puja.first;
        Link current1= subastas.first;
        while(current1!= null){
            while(current != null){
                
                if(current.puja.getUsuario().equals(usuario)){
                  resultado.insertFirst(current1.subasta);
                }
                else 
                      current= current.next;
            }
                      current1= current1.next;
        }
        return resultado;
      
	}
/**
 * Método mostrarSubastasGanadasPorUsuario: Permite mendiante la lista que almacena las subastas y con ayuda de las pujas realizadas por el usuario obtener las subastas ganadas por el ususario.
 * @param usuario Indica en el metodo el usuario el cual gano la subasta
 * @return el resultado de las subastas que gano el ususario.
 */
	public LinkListSubasta mostrarSubastasGanadasPorUsuario(Usuario usuario){
		LinkListSubasta resultado = new LinkListSubasta();
                Link current1= subastas.first;
                while(current1!=null){
		
		  if(current1.subasta.getGanador().getUsuario().equals(usuario)){
			  resultado.insertFirst(current1.subasta);
		  }	
                  else 
                      current1= current1.next;
                }
		return resultado;
                
        }
/**
 * Método buscarUsuario: Nos permite mediante los objetos de tipo Usuario buscar un nombre en la coleccion y asi comparar el nombre buscado con uno existente en la coleccion para obtener el resultado de la busqueda y el correspondiente nombre de usuario. 
 * @param nombre Nos indica en el metodo el nombre del usuario a buscar
 * @return Retorna el nombre del usuaario buscado dentro de la coleccion en caso de que no exista arrojara un null. 
 */

	public Usuario buscarUsuario(String nombre) {
           Usuario usuario = usuarios.buscarUsuario(nombre);
           return usuario;
          
	}
/**
 * Método buscarSubasta: permite mediante los objetos de tipo Subasta buscar un nombre en la lista y asi comparar el nombre buscado con uno existente para obtener el resultado de la busqueda y el correspondiente nombre de la subasta. 
 * @param nombre Nos indica en el metodo el nombre del usuario el cual participo o es dueño de una subasta.
 * @return Retorna el nombre de la subasta buscada dentro de la coleccion en caso de que no exista arrojara un null.
 */
	public Subasta buscarSubasta(String nombre) {
		Subasta subasta = subastas.buscarSubasta(nombre);
		return subasta;
	}		
	
}