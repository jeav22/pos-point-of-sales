package basesDatos.subastas.modelo;

import java.io.Serializable;
import bases.Datos.subastas.BaseEstructura.LinkListSubasta;
/**
* Clase Usuario: Clase que permite gestionar los usuarios que van a crear con sus respectivos datos
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/

public class Usuario implements Serializable{
    /**
 * Atributos de la clase SistemaSubasta:
 *nombre: String que contiene el nombre del usuario.
 * credito: double que contiene el credito del usuario.
 * propietario: objeto de la lista de subasta que sabe el propietario de cierta subasta.
 */ 
    private String nombre;
    private double credito;
    private boolean estado;
    private LinkListSubasta propietario;

    /**
     * Constructor de la clase Usuario
     * @param nombre Nombre de la persona a participar en la subasta
     * @param credito Monto total de dinero que tiene la persona
     * @param estado estado del usuario
     */
    public Usuario(String nombre, double credito, boolean estado) {
        this.nombre = nombre;
        this.credito = credito;
        this.estado = estado;
        this.propietario = new LinkListSubasta();
    }

    /**
     * Método getNombre: Permite obtener el nombre del usuario
     * @return el nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *Método setNombre: Permite enviar el nombre del usuario
     *@param nombre recibe como parametro el nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *Método getCredito: Permite obtener el valor del credito del usuario.
     *@return el credito que tiene el usuario
     */
    public double getCredito() {
        return credito;
    }

    /**
     * Método setCredito: Permite enviar el credito del usuario
     * @param credito recibe como parametro el credito del usuario
     */
    public void setCredito(double credito) {
        this.credito = credito;
    }
    /**
     * Método getPropietario: Obtiene a través de una lista de objetos de tipo Subasta obtener el propietario de dicha Subasta.
     * @return retorna el propitario de la subasta
     */
    public LinkListSubasta getPropietario() {
        return propietario;
    }
    /**
     * Método addPropietario: Nos permite añadir un propietario a la Subasta
     * @param propietario Indica en el metodo el propietario a agregar.
     */
    public void addPropietario(Subasta propietario) {
        this.propietario.insertFirst(propietario);
    }
    /**
     * Método incrementarCredito: Nor permite incrementar el valor del credito del Usuario
     * @param monto Indica en el metodo el monto a incrementar
     * @return retorna verdadero al incrementar el credito del usuario
     */
    public boolean incrementarCredito(double monto){
        this.credito+=monto;
        return true;
    }
    /**
     * Método incrementarCredito: Nor permite decrementar el valor del credito del Usuario
     * @param monto Indica en el metodo el monto a decrementar.
     * @return  retorna verdadero al decrementar el credito del usuario
     */
    public boolean decrementarCredito(double monto){
        this.credito-=monto;
        return true;
    }	
    /**
     * metodo isEstado : muestra el estado del usuario
     * @return estado del usuario 
     */
    public boolean istEstado(){
        return estado;
    }
     /**
     * metodo setEstado : cambia el estado del usuario 
     * @param estado en que estado se encuentra el usuario
     */
    public void setEstado(boolean estado){
        this.estado=estado;
    }
     /**
     * metodo getTamaño : muestra el tamaño del nombre que ingresa el usuario
     * @return tamañp del nombre del usuario
     */
    public int getTamaño() {
        return getNombre().length()*2 + 2 + 4 + 1;
    }
}