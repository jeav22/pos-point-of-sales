package basesDatos.subastas.modelo;

import java.io.Serializable;

/**
* Clase Puja: Contiene los elementos de una Puja para una determinada Subasta 
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class Puja implements Serializable{
/**
* Atributos de la Clase puja:
* valor: De tipo Double, su modificador de acceso es privado.    
* usuario: Es un objeto de la clase Usario, su modificador de acceso es privado. 
*/    
	private double valor;
	private Usuario usuarios;
/**
 * Contructor de la clase Puja:
 * @param valor indica en el método el monto del valor con el cual entrara a la puja. 
 * @param usuario indica en el método el usuasrio que realiza la puja.
 */	

	public Puja(double valor, Usuario usuario) {
		this.valor = valor;
		this.usuarios = usuario;
	}
/**
 * Método getValor: Permite obtener un valor.
 * @return Valor obtenido
*/

	public double getValor() {
		return valor;
	}
/**
 * Método setValor: Nos permite enviar un valor.
 * @param valor Parametro para indicar el valor a enviar 
 */        

	public void setValor(double valor) {
		this.valor = valor;
	}
/**
 * Método getUsuario: Nos permite obtener un usuario.
 * @return Usuario obtenido  
 */

	public Usuario getUsuario() {
		return usuarios;
	}
/**
 * Método setUsuario: Nos permite enviar un usuario.
 *@param usuario  Paramatro para indicar el usuario a enviar
 */  
	public void setUsuario(Usuario usuario) {
		this.usuarios = usuario;
	}	
}