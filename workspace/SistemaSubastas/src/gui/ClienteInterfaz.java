/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
/**
* Clase ClienteInterfaz: Es la clase que relaciona al usuario con el programa por medio de la reperesentacion del programa graficamente
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class ClienteInterfaz extends JFrame {
      /**
 * Atributos de la clase Servidor Interfaz:
 * listarPujas:tabla dentro de la interfaz
 * subastas:panel dentro de la interfaz
 * detalles:panel dentro de la interfaz
 * listaSubasta:lista dentro de la interfaz
 * nombreSubasta:label dentro de la interfaz
 * valorSubasta:label dentro de la interfaz
 * estado:label dentro de la interfaz
 * ganador:label dentro de la interfaz
 * crearSubasta:boton dentro de la interfaz
 * pujar:boton dentro de la interfaz
 * cerrarSubasta:boton dentro de la interfaz
 * iniciar: item dentro de la interfaz
 * registrarse:item dentro de la interfaz
 * aumentarCredito:item dentro de la interfaz
 * cerrar:item dentro de la interfaz
 * subastasGanadas:item dentro de la interfaz
 * subastasCreadas:item dentro de la interfaz
 * todas:item dentro de la interfaz
 * registro:item dentro de la interfaz
 * menu: barra de menu dentro de la interfaz
 * archivo: menu de la interfaz
 * filtro:menu de la interfaz
 * herramienta:menu de la interfaz
 * valorPuja: objetos de la clase Spinner
 * area: area de texto de la interfaz
 * usuario: nombre del usuario
 * subasta: nombre de la subasta
 * respuesta: mensaje respuesta 
 * credito: credito del usuario
 */  
    private JMenuBar menu;
    private JMenu archivo, filtro,herramienta;
    public JMenuItem iniciar, registrarse, aumentarCredito, cerrar
            ,subastasGanadas, subastasCreadas, todas,registro;
    private JPanel subastas, detalles;
    public JButton crearSubasta, pujar, cerrarSubasta;
    public JLabel nombreSubasta, valorSubasta, estado, ganador;
    public JSpinner valorPuja;
    public JList listaSubastas, listaPujas;
    public JTextArea area=null; 
    public DefaultListModel dlm;
    
    public String usuario, subasta;
    public String seteado="5",respuesta;
    public double credito;
     /**
 * Contructor de la clase ServidorInterfaz
 */
    public ClienteInterfaz() {
        iniciar();
    }
     /**
 * metodo iniciar: inicia los componentes de la interfaz del cliente
 */
    public void iniciar(){   
        usuario="null";     
        setLayout(null);
        menu = new JMenuBar();
        setJMenuBar(menu);
                
        area=new JTextArea();
        area.setEditable(false);
        JScrollPane scroll = new JScrollPane(area);
        scroll.setBounds(700, 0, 185, 400);
        add(scroll);

        archivo = new JMenu("Arhivo");
        filtro = new JMenu("Filtro");
        herramienta= new JMenu("Herramienta");
        menu.add(archivo);
        menu.add(filtro);
        menu.add(herramienta);

        iniciar = new JMenuItem("Iniciar sesion");        
        archivo.add(iniciar);

        registrarse = new JMenuItem("Registrarse");        
        archivo.add(registrarse);

        aumentarCredito = new JMenuItem("Aumentar credito");
        aumentarCredito.setEnabled(false);
        archivo.add(aumentarCredito);
        
        crearSubasta = new JButton("Crear Subasta");
        crearSubasta.setBounds(25, 365, 150, 30);
        crearSubasta.setEnabled(false);
        
        pujar = new JButton("Pujar");
        pujar.setBounds(400, 80, 80, 20); 
        pujar.setEnabled(false); 
        
        cerrarSubasta = new JButton("Cerrar Subasta");
        cerrarSubasta.setBounds(20, 80, 120, 20);
        cerrarSubasta.setEnabled(false);
        
        subastasGanadas = new JMenuItem("Subastas ganadas");
        subastasGanadas.setEnabled(false);
        
        subastasCreadas = new JMenuItem("Subastas Creadas");
        subastasCreadas.setEnabled(false);
        
        todas = new JMenuItem("Todas las subastas");
        
        filtro.add(subastasGanadas);
        filtro.add(subastasCreadas);
        filtro.add(todas);
        
        registro= new JMenuItem("Mostrar registro");
        registro.addActionListener(new ActionListener() {
            /**
            * metodo actionPerformed:  permite procesar una accion del usuario
 * @param e la accion del usuario
 */
            @Override
            public void actionPerformed(ActionEvent e) {
                if(getSize().equals(new Dimension(706, 452))){
                    setSize(890, 452);
                    registro.setText("Ocultar registro");
                }else{
                    setSize(706, 452);
                    registro.setText("Mostrar registro");
                }
            }
        });
        herramienta.add(registro);
        
        cerrar = new JMenuItem("Cerrar Sesion");
        cerrar.setEnabled(false);
        
        archivo.add(cerrar);
        
        nombreSubasta = new JLabel("::: Aqui va el nombre :::");
        nombreSubasta.setBounds(20, 10, 350, 50);
        nombreSubasta.setFont(new java.awt.Font("Arial Black", 0, 25));
        
        valorSubasta = new JLabel("$ 20'000.000");
        valorSubasta.setBounds(375, 25, 115, 50);
        valorSubasta.setFont(new java.awt.Font("Arial Black", 0, 16));
        
        ganador = new JLabel("aqui ganador");
        ganador.setBounds(375, 0, 115, 50);
        ganador.setFont(new java.awt.Font("Arial Black", 0, 16));
        
        estado = new JLabel("Estado de la subasta");
        estado.setBounds(150, 80, 150, 20);   
        estado.setFont(new java.awt.Font("Arial Black", 0, 12));
        
        valorPuja = new JSpinner();
        valorPuja.setBounds(315, 80, 80, 20);
                
        dlm=new DefaultListModel();
        listaSubastas = new JList();
        listaSubastas.setModel(dlm);
        listaSubastas.setBounds(20, 20, 160, 335);
        
        listaPujas = new JList();
        listaPujas.setBounds(20, 120, 460, 260);
        
        subastas = new JPanel();
        subastas.setLayout(null);
        subastas.setBackground(Color.GRAY);
        subastas.setBounds(0, 0, 200, 400);
        subastas.add(listaSubastas);
        subastas.add(crearSubasta);
        add(subastas);
        
        detalles = new JPanel();
        detalles.setLayout(null);
        detalles.setBackground(Color.LIGHT_GRAY);
        detalles.setBounds(200, 0, 500, 400);
        detalles.add(nombreSubasta);
        detalles.add(valorSubasta);
        detalles.add(valorPuja);
        detalles.add(ganador);
        detalles.add(pujar);
        detalles.add(listaPujas);
        detalles.add(cerrarSubasta);
        detalles.add(estado);
        add(detalles);
        
        setBounds(0, 0, 706, 452);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        setTitle("::: Sistema Subasta :::");
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);        
    }
    /**
    * metodo procesar: permite procesar los mensajes que entran y salen del cliente
 * @param mensaje mensaje del cliente
 * @param respuesta respuesta del cliente
 */
    public void procesar(String mensaje,String respuesta) throws InterruptedException{
        if(mensaje.startsWith("5")||mensaje.startsWith("9")||mensaje.startsWith("8")||mensaje.startsWith("4")){
            String[] m=respuesta.split(",");
            dlm=new DefaultListModel();
            for (int i = 0; i < m.length; i++) {
                dlm.addElement(m[i]);
            }
            listaSubastas.setModel(dlm);
            listaSubastas.setSelectedIndex(0);
        }else if(mensaje.startsWith("0") || mensaje.startsWith("1,")){
            if(respuesta.equalsIgnoreCase("true")){
                aumentarCredito.setEnabled(true);
                cerrar.setEnabled(true);
                iniciar.setEnabled(false);
                pujar.setEnabled(true);
                crearSubasta.setEnabled(true);
                registrarse.setEnabled(false);
                subastasGanadas.setEnabled(true);
                subastasCreadas.setEnabled(true);
                setTitle("::: Sistema Subasta ::: - "+usuario);
            }else{
                JOptionPane.showMessageDialog(null, "Este usuario no existe \no ya se encuentra activo", "Advertencia", 2);
            }
        }else if(mensaje.startsWith("12")){
            String[] k=respuesta.split("/-/");
            String[] m= k[0].split("," );
            nombreSubasta.setText(m[0]);
            valorSubasta.setText(m[1]);            
            estado.setText(m[2]);
            ganador.setText(m[3]);
            valorPuja.setValue((Double.parseDouble(valorSubasta.getText())+1));
            if(usuario.equals(m[4])){
                cerrarSubasta.setEnabled(true);
            }else{
                cerrarSubasta.setEnabled(false);
            }
            if(m[2].equalsIgnoreCase("true")){
               pujar.setEnabled(true);
            }else{
                pujar.setEnabled(false);
            }
            
            m=k[1].split(",");
            dlm=new DefaultListModel();
            for (int i = 0; i < m.length; i++) {
                dlm.addElement(m[i]);
            }
            listaPujas.setModel(dlm);
        }else if(mensaje.startsWith("11")){          
            if (respuesta.equalsIgnoreCase("true")) {
                cerrar.setEnabled(false);
                aumentarCredito.setEnabled(false);
                subastasGanadas.setEnabled(false);
                subastasCreadas.setEnabled(false);
                crearSubasta.setEnabled(false);
                iniciar.setEnabled(true);
                registrarse.setEnabled(true);
                setTitle("::: Sistema Subasta :::");
            }            
        }else if(mensaje.startsWith("6")){
            if(respuesta.equalsIgnoreCase("false")){
                JOptionPane.showMessageDialog(null, "Su puja no ha sido aceptada", "Advertencia", 2);
            }else{                
                int aux=listaSubastas.getSelectedIndex();
                if(aux<=0){
                    listaSubastas.setSelectedIndex(aux+1);
                }else{
                    listaSubastas.setSelectedIndex(aux-1);
                }
                listaSubastas.setSelectedIndex(aux);
            }
        }else if(mensaje.startsWith("7")){
            if(respuesta.equalsIgnoreCase("true")){
                cerrarSubasta.setEnabled(false);
                pujar.setEnabled(false);
                estado.setText("false");
            }
        }
    }    
}
