/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
* Clase ServidorInterfaz: Es la clase que relaciona al servidor con el programa por medio de la interfaz grafica
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class ServidorInterfaz extends JFrame{
    /**
 * Atributos de la clase Servidor Interfaz:
 * pujasRealizadas:tabla dentro de la interfaz
 * pujasGanadas:tabla dentro de la interfaz
 * clientes:panel dentro de la interfaz
 * datosClientes:panel dentro de la interfaz
 * listaClientes:lista dentro de la interfaz
 * subastasCreadas:lista dentro de la interfaz
 * credito:label dentro de la interfaz
 * titulo1:label dentro de la interfaz
 * nombre:label dentro de la interfaz
 * titulo2:label dentro de la interfaz
 * titulo 3:label dentro de la interfaz 
 * estado:label dentro de la interfaz
 * eliminarUsuario:boton dentro de la interfaz
 * cerrarSesion:boton dentro de la interfaz
 * area:boton dentro de la interfaz
 * nombres: arreglo  de nombres dentro de la interfaz
 */  
    
    public JPanel clientes, datosClientes;
    public JList listaClientes, subastasCreadas, pujasGanadas;
    public JLabel credito, titulo1, nombre, titulo3, estado;
    public JButton cerrarSesion;
    public JTextArea area;
    public String[] nombres=null;
    public DefaultListModel dlm;
    /**
 * Contructor de la clase ServidorInterfaz
 */
    public ServidorInterfaz(){
        setLayout(null);
        listaClientes = new JList();
        listaClientes.setBounds(20, 20, 110, 380);
                
        nombre = new JLabel("Aqui el nombre");
        nombre.setFont(new java.awt.Font("Arial Black", 0, 25));
        nombre.setBounds(20, 20, 250, 30);
        
        estado = new JLabel("Estado");
        estado.setFont(new java.awt.Font("Arial Black", 0, 15));
        estado.setBounds(40, 410, 100, 20);
        
        cerrarSesion = new JButton("Cerrar Sesión");
        cerrarSesion.setBounds(280, 410, 120, 20);
        
        credito = new JLabel("Credito : "+"20.000.000");
        credito.setFont(new java.awt.Font("Arial Black", 0, 15));
        credito.setBounds(320, 25, 250, 20);
              
        titulo1 = new JLabel("Subastas Creadas:");
        titulo1.setFont(new java.awt.Font("Arial Black", 0, 15));
        titulo1.setBounds(20, 60, 200, 30);
        
        subastasCreadas = new JList();
        subastasCreadas.setBounds(20, 100, 160, 300);
        
        titulo3 = new JLabel("Pujas Ganadas:");
        titulo3.setFont(new java.awt.Font("Arial Black", 0, 15));
        titulo3.setBounds(220, 60, 200, 30);
                 
        pujasGanadas = new JList();
        pujasGanadas.setBounds(200, 100, 280, 300);
        
        area=new JTextArea();
        JScrollPane scroll = new JScrollPane(area);
        scroll.setBounds(650, 0, 220, 445);
        add(scroll);
        
        clientes = new JPanel();
        clientes.setLayout(null);
        clientes.add(listaClientes);
        clientes.setBackground(Color.GRAY);
        clientes.setBounds(0, 0, 150, 450);
        this.add(clientes);
        
        datosClientes = new JPanel();
        datosClientes.setLayout(null);
        datosClientes.add(nombre);
        datosClientes.add(estado);
        datosClientes.add(cerrarSesion);
        datosClientes.add(credito);
        datosClientes.add(titulo1);
        datosClientes.add(subastasCreadas);
        datosClientes.add(titulo3);
        datosClientes.add(pujasGanadas);
        datosClientes.setBackground(Color.LIGHT_GRAY);
        datosClientes.setBounds(150, 0, 500, 450);
        this.add(datosClientes);
                
        setBounds(20, 20, 885, 480);
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("::: Servidor :::");
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);        
    }
}
