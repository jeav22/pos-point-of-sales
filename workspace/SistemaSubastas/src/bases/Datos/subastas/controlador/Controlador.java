package bases.Datos.subastas.controlador;

import bases.Datos.subastas.BaseEstructura.LinkListSubasta;
import bases.Datos.subastas.BaseEstructura.LinkListUsuario;
import java.io.IOException;
import basesDatos.subastas.modelo.SistemaSubasta;
import basesDatos.subastas.modelo.Subasta;
import basesDatos.subastas.modelo.Usuario;
import basesDatos.subastas.persistencia.BaseDatos;
import basesDatos.subastas.red.Servidor;
/**
* Clase Controlador: Es la clase encargada de conectar cada una de las demás capas de la aplicación permitiendo la manipulación de los datos en el modelo y cómo el usuario puede interactuar en la vista.   
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author José Paredes
* @version 1
* @since SistemaSubastas 1
*/
public class Controlador extends Thread{
    
    private LinkListUsuario usuarios;
    private LinkListSubasta subastas;
    private BaseDatos base;
/**
 * Atributos de la clase Controlador: 
 * vista: Objeto de tipo vista.
 * modelo:Objeto de tipo Sistema subastas
 * Estos dos atributos nos permitiran manipular el modelo de la aplicación y la vista.
 * 
 */   

    private Servidor servidor;
    private SistemaSubasta modelo;
/**
 * Constructor de la clase Controlador
 * @param servidor Parametro para indicar el uso del servidor en el controlador
 * @param modelo Parametro para indicar el uso de la servidor en el controlador
 */	
    public Controlador(Servidor servidor, SistemaSubasta modelo) {
        this.servidor = servidor;
        this.modelo = modelo;
        this.servidor.setControlador(this);
    } 
/**
 * Metodo crearUsuario: Permite crear un usuario utilizando la vista para que la persona interactue con la aplicación y cree dicho Usuario.
     * @return  true si se pudo crear el usuario
     * @throws java.io.IOException
 */
    public boolean crearUsuario() throws IOException {
        return servidor.crearUsuario();		
    }
/**
 *Método guardarUsuario: A través del modelo permite añadir los usuarios en él. 
 * @param usuario Parametro necesario ya que se debe conocer el usuario.
 */
    public void guardarUsuario(Usuario usuario) {
        modelo.addUsuarios(usuario);
    }
/**
 * Método listarUsuarios: Permite a través de la vista, mostrar los ususarios que existen.
     * @return la lista de usuarios
 */
    public String listarUsuarios() {
        LinkListUsuario usuarios =modelo.getUsuarios();
        return servidor.listarUsuario(usuarios);		
    }
/**
 * Método crearSubasta: Permite a través de la vista crear una subasta.
     * @throws java.io.IOException
 */
    public void crearSubasta() throws IOException {
        servidor.crearSubasta();		
    }
/**
 * Método listarSubastas: Permite a través de la vista, mostrar las subastas que se encuentran dentro de la coleccion de objetos tipo Subasta.
     * @return las listas de las subastas
 */
    public String listarSubastas() {
        LinkListSubasta subastas =modelo.getSubastas();
        return servidor.listarSubastas(subastas);
    }
/**
 * Método guardarSubasta: Permite mediante la manipulación del modelo de la aplicación guardar las subastas creadas.
 * @param subasta Parametro que indica la subasta a guardar
 */
    public void guardarSubasta(Subasta subasta) {
        modelo.addSubastas(subasta);
    }
/**
* Método buscarUsuario: Permite mediante el modelo buscar un ususario. 
 * @param nombre Este parametro es necesario para reoonocer el nombre del usuario.
 * @return Retorna el usuario buscando dentro del modelo.
 */
    public Usuario buscarUsuario(String nombre) {
        Usuario usuario=this.modelo.buscarUsuario(nombre);
        return usuario;
    }
/**
 * Método pujarSubastas: Permite mediante la combinación del modelo obtener las Subastas y enviarle a la vista dichas subastas para permitir realizar pujas.
     * @return si se pudo pujar la subasta
     * @throws java.io.IOException
 */
    public boolean pujarSubastas() throws IOException {
        return servidor.pujarSubastas();
    }
/**
 * Método buscarSubasta: Permite a través del modelo buscar una subasta especifica.
 * @param nombre recibe como parametro el nombre de la subasta
 * @return Nos retorna la subasta buscada
 */
    public Subasta buscarSubasta(String nombre) {
        Subasta subasta=this.modelo.buscarSubasta(nombre);
        return subasta;
    }
/**
 * Método Pujar: Permite Pujar por una subasta. 
 * @param usuario Parametro necesario para conocer quien esta haciendo la puja.
 * @param subasta1 Parametro para la subasta correspondiente 
 * @param valor Parametro necesario para saber el valor pujado.
 * @return Retorna mediante un tipo de dato booleano si se puede pujar o no.
 */
    public boolean pujar(Usuario usuario, Subasta subasta1, double valor) {		
        return subasta1.registrarPuja(valor, usuario);
    }
/**
 * Método Pujar: Permite pujar por una subasta pero ahora ya no se tiene en cuenta el valor sino que permite pujar automaticamente.
 * @param usuario Parametro necesario el nombre del Usuario que realiza la puja.
 * @param subasta1 Parametro necesario ya que utilizamos la subasta para este método. 
 * @return Retorna mediante un tipo de dato booleano si se puede pujar o no.
 */
    public boolean pujar(Usuario usuario, Subasta subasta1) {
        if (subasta1.registrarPuja(usuario)) {
            return true;	
        }else{
            return false;		
        }
    }
/**
 * Método cerrarSubastas: Permite al usuario mediante la vista cerrar las subastas correspondientes al propietario.
     * @return  si se pudo cerrar la subasta
     * @throws java.io.IOException
 */
    public boolean cerrarSubastas() throws IOException {
        return servidor.cerrarSubastas();
    }
/**
 * Método cerrarSubasta: Permite al usuario, propietario de la subasta cerrar dicha subasta. 
 * @param subasta Subasta a cerrar por el propietario
 * @return Retorna a través de un tipo de dato booleano el cierre de la subasta. Si fue cerrada o no.
 */
    public boolean cerrarSubasta(Subasta subasta) {
        if (subasta.cerrarSubasta()) {
            return true;
        }
        return false;
    }
/**
 * Método mostrarSubastasUsuario: Permite mediante la vista listar las subastas en las cuales los ususarios participaron. 
     * @return  las subastas creadas
 */
    public String mostrarSubastasCreadas() {
        return servidor.listarSubastasCreadas();		
    }
/**
 * Método mostrarSubastasUsuario: Permite mediante la vista listar las subastas en las cuales los ususarios participaron. 
     * @return  las subastas ganadas
 */
    public String mostrarSubastasGanadas() {
        return servidor.listarSubastasGanadas();		
    }
/**
 *Método mostrarPujasUsuario: Mediante la vista, permite mostrar las pujas realizadas por el usuario. 
 */
    public void mostrarPujasUsuario() {
        servidor.listarPujasUsuario();
    }
/**
 * Método listarPujas: Permite listar las pujas almacenadas en la coleccion de objetos tipo Subasta mediante la utilización del modelo.
 * @return retornar las subastas almacenadas dentro de la coleccion 
 */
    public LinkListSubasta listarPujas() {
        LinkListSubasta subastas =modelo.getSubastas();	
        return subastas;
    }
/**
 * Método cargarSistema: Nos permite recuperar la información guardada en el modelo en interacciones con la aplicacion anteriormenet.
 * @throws ClassNotFoundException  Excepcion para pero señalar que no hay una definición para la clase con el nombre especificado.
 */
    public void cargarSistema() throws ClassNotFoundException{
            this.modelo= null;
            base= new BaseDatos();	
            this.modelo=base.leerSubastas("database.bin");
	}
/**
 * Método guardarSistema: Nos permite guardar la información del sistema de Subastas almacenados en el modelo para la reutilización de este en una nueva ocasión 
 * @throws IOException Excepcion para indicar un fallo a la hora de guardar el sistema.
 */
    public void guardarSistema() throws IOException{
        SistemaSubasta baseDatos= modelo;
        base = new BaseDatos();
        base.escribirSubastas(baseDatos);	
    }
/**
 * Método actualizarCredito: Nos permite mediante la vista pedirle al usuario que actualice su credito.
     * @return  true si se pudo actualizar el credito
     * @throws java.io.IOException
 */
    public boolean actualizarCredito() throws IOException {
        return servidor.actualizarCredito();
    }

    public boolean cerrarSesion() throws IOException {
        return servidor.cerrarSesion();
    }

    public boolean cerrarUsuario(Usuario usuario) {
        if(usuario!=null){
            usuario.setEstado(false);
            return true;
        }else{
            return false;
        }
    }

    public boolean iniciarSesion(Usuario usuario) {
        if(usuario.istEstado()==true){
            return false;
        }else{
            usuario.setEstado(true);
            return usuario.istEstado();
        }
    }

    public String mostrarPujasSubasta() {
        return servidor.listarPujasSubasta();
    }
}