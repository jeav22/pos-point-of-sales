/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases.Datos.subastas.BaseEstructura;

import basesDatos.subastas.modelo.Usuario;
import java.io.Serializable;

/**
* Clase LinkListUsuario: Es la clase encargada de estructurar la entrada de un nuevo usuario al sistema y hacer manejo de estos.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class LinkListUsuario implements Serializable{
    /**
 * Atributos de la clase LinkListUsuario:
 * first: Objeto de la clase link.
 */
   public Link first;           

   /**
 * Constructor de la clase LinListUsuario
 */ 
   public LinkListUsuario()             
      {
      first = null;               
      }
    /**
* Metodo isEmpty: Permite evaluar si la lista de las pujas esta vacia.
     * @return retorna nulo si el first de la lista no tiene nada. 
 */
   public boolean isEmpty()       
      {
      return (first==null);
      }
                                 /**
 * Metodo insertfirst: Permite insertar en la lista de usuario un nuevo elemento de este tipo. 
 * @param usuario recibe el usuario que se quiere agregar.
 */
   public void insertFirst(Usuario usuario)
      {                           
      Link newLink = new Link(usuario);
      newLink.next = first;      
      first = newLink;          
      }
/**
 * Metodo deleteFirst: Permite eleminar elemento de la lista.   
 * @return  
 */
   public Link deleteFirst()     
      {                           
      Link temp = first;         
      first = first.next;         
      return temp;                
      }
/**
 * Metodo mostarUsuario: Permite mostrar los usuarios que estan dentro de la lista. 
 */
   public void mostarUsuarios()
      {
      System.out.print("List (first-->last): ");
      Link current = first;       
      while(current != null)      
         {
         current.displayLinkUsuario();
         current = current.next;  
         }
      System.out.println("");
      }
   /**
 * Metodo getUsuario: Permite mandar el nombre del usuario cuando se necesite. 
 * @param usuario recibe el un onajeto usuario que despues devolvera. 
     * @return  retorna el ultimo usuario que se inserto en la lista 
 */
   public Usuario getUsuario(Usuario usuario)
    {
        Link current = first;
        while(current.next!= null){
            if(current.usuario.equals(usuario)){
                break;
            }else{
                current = current.next;
            }
        }
        return current.usuario;
    }
   /**
 * Metodo buscarUsuario: Permite buscar un usuario dentro de su respectiva lista. 
 * @param usuario recibe el nombre del usuario que se quiere encontrar.
     * @return retorna nulo si no encuentra al usuario, si lo encuentra retorna el usuario que se estaba buscando.
 */
   public Usuario buscarUsuario(String usuario)
    {
        Link current = first;
        if(current != null){
            while(current!= null){
                if(current.usuario.getNombre().equals(usuario)){
                    return current.usuario;
                }else{
                    current = current.next;
                }
            }
        }
        return null;
    }
    
   } 
