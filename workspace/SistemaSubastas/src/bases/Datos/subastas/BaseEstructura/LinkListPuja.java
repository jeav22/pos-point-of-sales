/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases.Datos.subastas.BaseEstructura;

import basesDatos.subastas.modelo.Puja;
import java.io.Serializable;
/**
* Clase LinkListPuja: Es la clase encargada de estructurar la entrada de una nueva puja al sistema y hacer manejo de estas.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class LinkListPuja implements Serializable{
    /**
 * Atributos de la clase LinkListPuja:
 * first: Objeto de la clase link.
 */
    
    public Link first;           
/**
 * Constructor de la clase LinListUsuario
 */ 
   public LinkListPuja()             
      {
        first = null;               
      }
 /**
* Metodo isEmpty: Permite evaluar si la lista de las pujas esta vacia . 
     * @return retorna nulo si el first de la lista no tiene nada. 
 */
   public boolean isEmpty()       
      {
        return (first==null);
      }
 /**
 * Metodo insertfirst: Permite insertar en la lista de puja un nuevo elemento de este tipo. 
 * @param puja recibe la puja que se quiere agregar.
     * @return retorta true si el sistema logra insertar la puja. 
 */     
   public boolean insertFirst(Puja puja)
      {                           
        Link newLink = new Link(puja);
        newLink.next = first;      
        first = newLink;          
        return true;
      }
/**
 * Metodo deleteFirst: Permite eleminar elemento de la lista.   
 * @return  
 */
   public Link deleteFirst()     
      {                           
        Link temp = first;         
        first = first.next;         
        return temp;                
      }
/**
 * Metodo mostarUsuario: Permite mostrar las pujas que estan dentro de la lista. 
 */
   public void mostrarPujas()
      {
        System.out.print("List (first-->last): ");
        Link current = first;       
        while(current != null)      
           {
           current.displayLinkSubata();
           current = current.next;  
           }
        System.out.println("");
      }
     /**
 * Metodo buscarPuja: Permite buscar una puja dentro de su respectiva lista. 
 * @param puja recibe el objeto puja que se quiere encontrar.
     * @return retorna nulo si no encuentra la puja, si lo encuentra retorna la puja que se estaba buscando.
 */
   public Puja buscarPuja(Puja puja)
    {
        Link current = first;
        while(current.next!= null){
            if(current.subasta.equals(puja)){
                break;
            }else{
                current = current.next;
            }
        }
        return current.puja;
    }
}
