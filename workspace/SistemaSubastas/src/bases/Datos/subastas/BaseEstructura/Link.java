/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases.Datos.subastas.BaseEstructura;

import basesDatos.subastas.modelo.Puja;
import basesDatos.subastas.modelo.Subasta;
import basesDatos.subastas.modelo.Usuario;
import java.io.Serializable;
/**
* Clase Link: Es la clase encargada de estructurar los datos que entraran.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class Link implements Serializable
   {
    /**
 * Atributos de la clase Link:
 * subasta: objeto de la clase Subasta.
 * Puja: objeto de la clase Puja.
 * next: objeto de la clase Link.
*/
   public Usuario usuario;    
   
   public Subasta subasta;
   public Puja puja;
   public Link next;              
/**
 * Constructor de la clase Link para el objeto usuario
     * @param usuario
 */ 
   public Link(Usuario usuario) 
      {
          this.usuario=usuario;
      }                           
   /**
 * Constructor de la clase Link para el objeto subasta
     * @param subasta
 */ 
   public Link(Subasta subasta) 
      {
          this.subasta=subasta;
      }             
   
   /**
 * Constructor de la clase Link para el objeto puja.
     * @param puja
 */ 
   public Link(Puja puja) 
      {
          this.puja=puja;
      }             
/**
* Metodo dicplayLinkUsuario: Permite imprimir por consola los datos del usuario como nombre y credito.
 */

   public void displayLinkUsuario()     
      {
        System.out.print("{ Usuario: " + usuario.getNombre() + ", Credito: " + usuario.getCredito() + "} ");
      }
/**
* Metodo dicplayLinkSubasta: Permite imprimir por consola los datos de la subasta como el producto y su respectivo propietario.
*/
   
   public void displayLinkSubata()     
      {
        System.out.print("{ Producto: " + subasta.getProducto() + ", Propietario: " + subasta.getPropietario() + "} ");
      }
   
/**
* Metodo isEmpty: Permite evaluar si la lista de las pujas esta vacia.
 */
   
   public void displayLinkPuja()     
      {
        System.out.print("{ Usuario: " + puja.getUsuario()+ ", puja: " + puja.getValor()+ "} ");
      }
   }  