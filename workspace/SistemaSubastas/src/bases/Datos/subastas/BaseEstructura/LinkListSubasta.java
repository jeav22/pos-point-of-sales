/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases.Datos.subastas.BaseEstructura;

import basesDatos.subastas.modelo.Subasta;
import java.io.Serializable;
/**
* Clase LinkListSubasta: Es la clase encargada de estructurar la entrada de una nueva subasta al sistema y hacer manejo de estas.
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/

public class LinkListSubasta implements Serializable{
    /**
 * Atributos de la clase LinkListSubasta:
 * first: Objeto de la clase link.
 */
   public Link first;           
/**
 * Constructor de la clase LinListSubasta
 */ 

   public LinkListSubasta()             
      {
      first = null;               
      }
/**
* Metodo isEmpty: Permite evaluar si la lista de las subasta esta vacia . 
     * @return retorna nulo si el first de la lista no tiene nada. 
 */

   public boolean isEmpty()       
      {
      return (first==null);
      }
   /**
 * Metodo insertfirst: Permite insertar en la lista de subasta un nuevo elemento de este tipo. 
 * @param subasta recibe el objeto subasta que se quiere agregar.
     */                              
   public void insertFirst(Subasta subasta)
      {                           
      Link newLink = new Link(subasta);
      newLink.next = first;      
      first = newLink;          
      }
/**
 * Metodo deleteFirst: Permite eleminar elemento de la lista.   
 * @return  
 */

   public Link deleteFirst()     
      {                           
      Link temp = first;         
      first = first.next;         
      return temp;                
      }
/**
 * Metodo listarSubastas: Permite mostrar las subastas que estan dentro de la lista. 
 */
   public void listarSubastas()
      {
      System.out.print("List (first-->last): ");
      Link current = first;       
      while(current != null)      
         {
         current.displayLinkSubata();
         current = current.next;  
         }
      System.out.println("");
      }
   /**
 * Metodo buscarSubasta: Permite buscar una subasta dentro de su respectiva lista. 
 * @param subasta recibe el objeto subasta que se quiere encontrar.
     * @return retorna nulo si no encuentra la subasta, si lo encuentra retorna la subasta que se estaba buscando.
 */
   public Subasta buscarSubasta(String subasta)
    {
        Link current = first;
        if(current != null){
            while(current!= null){
                if(current.subasta.getProducto().equals(subasta)){
                    return current.subasta;
                }else{
                    current = current.next;
                }
            }
        }
        return null;
    }
   
   }