/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases.Datos.persistenciaAleatoria;

import basesDatos.subastas.modelo.Subasta;
import basesDatos.subastas.modelo.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.*;

/**
* Clase ArchivoAleatorio: Es la clase que permite el manejo de un arhivo para guardar, eliminar o modificar datos del usuario
* @author Nicolás Chicuazuque
* @author Jorge Arenas 
* @author Paola Vargas 
* @author Alejandra Castillo
* @author Alejandro Rodriguez
* @version 2
* @since SistemaSubastas 
*/
public class ArchivoAleatorio implements Serializable{
    /**
     * atributos de la clase ArchivoAleatorio
     * flujo : objeto de la clase RandomAccessFile
     * numeroRegistros : numero de usuarios en el archivo
     * tamañoRegistro: tamaño maximo del dato que ingresa el usuario
     */
    private static RandomAccessFile flujo;
    private static int numeroRegistros;
    private static int tamañoRegistro = 80;
    
    
    
/**
     * metodo crearFile : crea el archivo donde se guardara la informacion
     * @param archivo el archivo existente 
     * @throws java.io.IOException
     */
    public static void crearFile(File archivo) throws IOException {
        if (archivo.exists() && !archivo.isFile()) {
            throw new IOException(archivo.getName() + " no es un archivo");
        }
        flujo = new RandomAccessFile(archivo, "rw");
        numeroRegistros = (int) Math.ceil(
                (double) flujo.length() / (double) tamañoRegistro);
    }
/**
     * metodo cerrar : cierra el archivo
     * @throws java.io.IOException
     */
    public static void cerrar() throws IOException {
        flujo.close();
    }
    /**
     * metodo getNumeroRegistros: obtiene el numero de registros
     * @return el numero de registros
     */
     public static int getNumeroRegistros() {
        return numeroRegistros;
    }
     /**
     * metodo crearUsuario : crea el usuario dentro del archivo
     * @param i  numero de registro
     * @param usuario nombre del usuario
     * @return si se puede crear el usuario
     * @throws java.io.IOException
     */
    public  boolean  crearUsuario(int i, Usuario usuario) throws IOException{
        if(i >= 0 && i <= getNumeroRegistros()) {
            if(usuario.getTamaño() > tamañoRegistro) {
                System.out.println("\nTamaño de registro excedido.");
            } else {
                flujo.seek(i*tamañoRegistro);
                flujo.writeUTF(usuario.getNombre());
                flujo.writeDouble(usuario.getCredito());
                flujo.writeBoolean(usuario.istEstado());
                return true;
            }
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
        }
        return false;
    }
    /**
     * metodo listarUsuario: muestra la lista de usuario
     * @param i el numero de registro
     * @return los usuarios de la lista
     * @throws java.io.IOException
     */
   public static Usuario listarUsuario(int i) throws IOException {
        if(i >= 0 && i <= getNumeroRegistros()) {
            flujo.seek(i * tamañoRegistro);
            return new Usuario(flujo.readUTF(), flujo.readDouble(), flujo.readBoolean());
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
            return null;
        }
    }
   /**
     * metodo crearSubasta: crea una subasta dentro del archivo
     * @param i numero de registro
     * @param subasta subasta a crear
     * @return  si la subasta se creo
     * @throws java.io.IOException
     */
    public  boolean  crearSubasta(int i, Subasta subasta) throws IOException{
        if(i >= 0 && i <= getNumeroRegistros()) {
            if(subasta.getTamaño() > tamañoRegistro) {
                System.out.println("\nTamaño de registro excedido.");
            } else {
                flujo.seek(i*tamañoRegistro);
                flujo.writeUTF(subasta.getProducto());
              flujo.writeUTF(subasta.getPropietario().getNombre());
                return true;
            }
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
        }
        return false;
    } 
    /**
     * metodo lsitarSubasta: muestra la lsita de subastas
     * @param i numero de registro
     * @return lista de subastas
     * @throws java.io.IOException
     */
    public static Subasta listarSubasta(int i ) throws IOException {
        
        if(i >= 0 && i <= getNumeroRegistros()) {
            flujo.seek(i * tamañoRegistro);
            return new Subasta(flujo.readUTF(), new Usuario (flujo.readUTF(), flujo.readDouble(), flujo.readBoolean()));
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
            return null;
        }
    }
    /**
     * metodo buscarUsuario: busca un usuario en el arhivo
     * @param buscado el nombre del usuario a buscar
     * @return el numero de registro del usuario
     * @throws java.io.IOException
     */
     public static int buscarUsuario(String buscado) throws IOException {
        Usuario p;
        if (buscado == null) {
            return -1;
        }
        for(int i=0; i<getNumeroRegistros(); i++) {
            flujo.seek(i * tamañoRegistro);
            p = listarUsuario(i);
            if(p.getNombre().equals(buscado) && p.istEstado()) {
                return i;
            }
        }
        return -1;
    }
     /**
     * metodo buscarSubasta: busca una subasta en el archivo
     * @param buscado el nombre de subasta a buscar
     * @return el numero de registro de la subasta
     * @throws java.io.IOException
     */
        public static int buscarSubasta(String buscado) throws IOException {
        Subasta p;
        if (buscado == null) {
            return -1;
        }
        for(int i=0; i<getNumeroRegistros(); i++) {
            flujo.seek(i * tamañoRegistro);
            p = listarSubasta(i);
            if(p.getProducto().equals(buscado)) {
                return i;
            }
        }
            System.out.println("quita este");
        return -1;
    }
}
